# Lesson 1
## 22/10/2015

## ex. 1
Given two integer numbers store them into two variables,
exchange the content of the two variables.
print the content of the two.


## ex. 2
Acquire an integer number from keyboard and print
the preceding and next number.


## ex. 3
Acquire an integer number from keyboard and print if it's positive or negative.
Also print its absolute value.


## ex. 4
Given a real number D calculate and print:
  - area of the square of side D
  - area of the circle of diameter D
  - area of the equilateral triangle of side D


## ex. 5
Given the equation ax+b=0 with a and b inserted with the keyboard,
determine the value of x -if it exists- that solves the equation.
