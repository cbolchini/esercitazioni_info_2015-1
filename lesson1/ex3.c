#include <stdio.h>

int main(int argc, char *argv[]){

    int n, abs;

    printf("Insert a number: ");
    scanf("%d", &n);

    printf("\n%d ", n);

    if ( n >= 0 ){
        printf("is positive\n");
        abs = n;
    } else {
        printf("is negative\n");
        abs = -n;
    }

    printf("The absolute value of %d is %d\n", n, abs);

    return 0;
}
