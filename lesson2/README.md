# Lesson 2
## 29/10/2015

## ex. 1
Scrivere un programma che acquisisce un numero intero compreso nell'intervallo [0, 2047] e finche' non e' tale lo richiede.
Il programma calcola e visualizza il valore corrispondente in base 2 sul numero minimo di bit.

Per esempio se il programma acquisisce 37, il programma visualizza 100101.


## ex. 2
Scrivere un programma che acquisisce un valore intero che rappresenta un numero naturale espresso nel sistema binario.
Il programma calcola e visualizza il corrispondente in base 10.

Per esempio se il programma acquisisce 1001, il programma visualizza 9.

Si assuma che l'utente inserisca sempre un valore valido.
(NOTA: e' possibile riutilizzare il programma precedente? se si, con quali modifiche?)

## ex. 3
Scrivere un programma che acquisisce una sequenza di *al più* 60 numeri interi, che si ritiene terminata quando l'utente
inserisce il valore 0 (che non fa parte della sequenza)
Il programma visualizza tutte le possibili coppie di valori.
Per esempio se l'utente inserisce
1 4 7 0
il programma visualizza
1 4
1 7
4 7
La coppia 1 4 è equivalente alla coppia 4 1 e non devono comparire entrambe.
Non deve comparire la coppia formata da due volte lo stesso valore (ad esempio 1 1).
Si ipotizzi che l'utente non inserisca più volte lo stesso valore.
Variante: fare in modo che nel caso in cui l'utente inserisca un valore già inserito, questo venga ignorato.

## ex. 4
Scrivere un programma che acquisisce una sequenza di *al più* 256 caratteri, che si ritiene terminata
quando l'utente inserisce il carattere '*'.
Il programma calcola il numero di volte che ogni carattere
che sia o una lettera maiuscola, o una lettera minuscola, compare nella sequenza.
Al termine il programma visualizza, per ogni carattere che compare almento una volta un istogramma fatto di *.

Ad esempio, se l'utente inserisce:
prova di lavoro*

il programma visualizza
a **
d *
l *
o ***
p *
r **
v *


*NOTA:* per chi volesse provare a collaudare il programma, l'acquisizione viene fatta mettendo la sequenza di caratteri,
uno dopo l'altro, e terminati dall'*, quindi digita invio

## homework
Implementare la seguente variante del [http://it.wikipedia.org/wiki/Cifrario_di_Cesare](cifrario di cesare)
Acquisire il plaintext (testo in chiaro) come sequenza di caratteri maiuscoli e spazi di *al massimo* 160 caratteri,
che si ritiene terminata dal carattere '*'. Assumere che non ci siano errori nei caratteri inseriti.

Il programma dovrà chiedere all’utente la chiave sotto forma di numero intero compreso tra 1 e 10.

Stampare il messaggio cifrato ottenuto nel seguente modo:
leggere il messaggio al contrario (partendo quindi dall'ultimo carattere fino ad arrivare al primo)
traslando ogni lettera di K posizioni 

Stampare il messaggio cifrato.
