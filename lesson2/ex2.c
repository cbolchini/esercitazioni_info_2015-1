#include <stdio.h>

#define B10 2
#define B2  10

int main(int argc, char *argv[])
{
    int bin, ndec, r, n, pown;

    printf("Inserisci un numero binario: ");

    scanf("%d", &ndec);

    bin = 0;
    n = ndec;
    pown = 1;

    while(n > 0){
        r = n % B2;
        n = n / B2;
        bin = bin + r*pown;
        pown = pown*B10;
    }



    printf("%d = %d\n", ndec, bin);

    return 0;
}
