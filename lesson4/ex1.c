#define MAX 500

int main(int argc, char* argv[]){
    int conteggio;
    int inParola;
    char parola[MAX+1];
    int i;

    gets(parola);

    inParola=0;
    conteggio=0;

    for(i = 0; parola[i]!='\0'; i++){
        if( parola[i] == ' '  ||
            parola[i] == '\t' ||
            parola[i] == '\n' ||
            parola[i] == '\r')
                if (inParola == 1){
                    inParola = 0;
                    conteggio++;
                }
        inParola = 1;
    }
    /*aggiungo 1 se la riga e' terminata da una parola*/
    if ( inParola )
        conteggio++;

    printf("%d",conteggio);
    return 0;
}
