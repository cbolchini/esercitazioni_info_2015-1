#include <stdio.h>

typedef struct rettangolo {
       float base, altezza;
} t_rettagolo;

int main(int argc, char *argv[]){
    int area, perimetro;
    t_rettangolo rett;

    scanf("%f %f", &rett.base, &rett.altezza);

    area = rett.base*rett.altezza;
    perimetro = (rett.base+rett.altezza)*2;

    printf("%f %f", area, perimetro);

    return 0;
}
