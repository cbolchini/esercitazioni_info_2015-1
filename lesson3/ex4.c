#include <stdio.h>

#define DIM 3

int main(int argc, char *argv[]){

    int Q[DIM][DIM];
    int i,j;
    int flag;
    int sum, sum2, prec;

    /* acquisizione */
    for(i = 0; i < DIM; i++){
        for(j = 0; j < DIM; j++){
            printf("Q[%d][%d] = ",i ,j);
            scanf("%d", &Q[i][j]);
        }
    }
    printf("\n");

    flag = 0;

    /* lavoriamo su righe e colonne */
    for(i = 0; (i < DIM) && !flag; i++){

        sum = 0;
        sum2 = 0;

        for(j = 0; (j < DIM) && !flag; j++){
            sum = sum + Q[i][j]; /*sum row*/
            sum2 = sum2 + Q[j][i]; /*sum col.*/
        }

        if( (sum!=sum2) || (i!=0 && j!=0 && sum!=prec ))
            flag = 1;

        prec = sum;
    }

    sum = 0;
    sum2 = 0;

    /* lavoriamo sulla diagonale */
    for(i = 0; i<DIM; i++){
        sum = sum + Q[i][i];
        sum2 = sum2 + Q[i][DIM-1-i];
    }

    if (sum != sum2 || sum != prec)
        printf("non e' magico!");
    else
        printf("il quadrato e' magico e la sua costante e' %d\n", sum);

    return 0;
}
